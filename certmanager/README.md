# Установка и конфигурация **cert-manager** installation с помощью Helm
### Добавляем репозиторий **cert-manager** и обоновляем его  

`helm repo add jetstack https://charts.jetstack.io`  
`helm repo update`  
- - -
### Устанавливаем нужный чарт, создаем **namespace**, добавляем необходимые CRD (**Issuer** и **ClusterIssuer**)  

`helm install cert-manager jetstack/cert-manager  --namespace cert-manager  --create-namespace --set installCRDs=true`
- - -

### Проверяем установку **cert-manager**  

`kubectl get pods --namespace cert-manager`  
- - -

### Деплоим **ingress-controller**
ссылка на деплой контроллеров
- - -

### Добавляем днс-запись на днс-сервере\провайдере
- - -

### Конфигурируем получение сертификата от Let's Encrypt
- создаем абстракцию *Issuer* для определения параметров получения сертификата:
  - сервер LE;
  - email;
  - имя секрета, в котором будет храниться сертификат;
  - тип проверки (http или dns)

<details>
  <summary>ext-issuer.yaml </summary>
    
  ```yaml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt-ext
spec:
  acme:
    # The ACME server URL
    server: https://acme-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: youremail@here.com
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: external-secret
    # Enable the HTTP-01 challenge provider
    solvers:
    - http01:
        ingress:
          class: nginx-ext
  ```
  
</details>  

*Применяем манифест*  
`kubectl create -f ext-issuer.yaml`

Если мы хотим использовать эту абстракцию для получения сертификатов не только в конкретном *namespace*, а *cluster*-wide, то нам следует поменять **kind**, вместо **Issuer** указать **ClusterIssuer**

Тип проверки будет зависеть от среды выполнения.
Есть 2 основных типа проверки:
- *http*
   - на веб-сервере специальный файл располгается по определенному пути, по которому происходит обращение и дальнейшая валидация и выдача сертификата.  
   минусы - не поддерживает wildcard-сертификаты, зависимость от файла;
- *dns*
    - на днс-сервере\у провайдера создается специальная txt-запись, LE запрашивает эту запись, если происходит корректный ответ, то процедура выдачи серитфиката проходит дальше.  
    минусы - данные от API провайдера подвержены риску кражи, провайдер не поддерживает API.
- - -
### Указываем секрет в **ingress**  
<details>
  <summary>ingress.yaml</summary>
    
  ```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: external-app
  namespace: external-app
  annotations:
    cert-manager.io/issuer: "letsencrypt-ext"
spec:
  ingressClassName: nginx-ext
  tls:
  - hosts:
    - external.domain.com
    secretName: external-secret
  rules:
  - host: external.app.com
    http:
      paths:
      - path: /
        pathType: ImplementationSpecific
        backend:
          service:
            name: external-service
            port:
              number: 80
  ```
  
</details>

Указав аннотацию `cert-manager.io/issuer` мы сообщаем, что хотим получить сертификат именно из этого центра сертификации, который мы указали ранее.  
**cert-manager** прочитает эти аннотации и использует для создания сертификата, который можно будет посмотреть с помощью команды  
`kubectl get certificate`.  
 Информацию о шагах получения сертификата можно будет посмотреть с помощью команды `kubectl describe certificate <cert-name>` в секции *Events*.
