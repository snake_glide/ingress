# ingress-controller's deployment

### Flow
- - -  
За основу взят базовый репозиторий [ingress-nginx](https://kubernetes.github.io/ingress-nginx), helm-чарты последней версии.


- - -
**Деплой контроллеров:**
- **charts/extapp**  (*простой чарт с манифестом внешних service и ingress для примера* );
- **charts/ingress-external** (*values для helm-чарта деплоя внешнего ingress-контроллера*);
- **charts/ingress-internal** (*values для helm-чарта деплоя внутреннего ingress-контроллера*);
- **charts/intapp**  (*простой чарт с манифестом внутренних service и ingress для примера*);
- - - 

1. Деплой контроллеров в кластер осуществляется с помощью **Helm**. Через файл *values.yml* для каждого деплоя контроллеров передаются необходимые параметры (взято [отсюда](https://kubernetes.github.io/ingress-nginx/#how-to-easily-install-multiple-instances-of-the-ingress-nginx-controller-in-the-same-cluster))
2. У каждого контроллера указывается параметр **IngressClass**, по которому происходит разделение обработки правилами **Ingress**.  
   В самих же правилах **Ingress**, в поле **ingressClassName** указывается имя необходимого ingress-controller`а.
- - - 
Деплой cert-manager:  
- **certmanager/ext-issuer** (*манифест ресурса Issuer для внешнего контроллера*);
- **certmanager/int-issuer** (*манифест ресурса Issuer для внутрннего контроллера*);
- **README.md** (*описание процесса получения сертификата*).
